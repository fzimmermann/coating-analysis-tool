classdef AluCoatImageProcessor < handle
        
    properties
        originalImage
        correctedImage
        imageHandle
        axes
        sharpnessRadius
        sharpnessAmount
    end
    
    methods
        function prc = AluCoatImageProcessor(ax)
            prc.axes = ax;
        end        

        function loadImage(prc, file)
            image = imread(file);
            if size(image,3) > 3
                % remove transpareny channel if there is one. 
                % We are only interested in the first three colour channels
                image(:,:,4:end) = []; 
            end
            prc.originalImage = rgb2gray(image);
            prc.correctedImage = prc.originalImage;
            prc.showImage();
        end

        function showImage(prc)
            if isempty(prc.imageHandle)
                prc.imageHandle = imshow(prc.correctedImage,'Parent', prc.axes);
            else
                prc.imageHandle.CData = prc.correctedImage;
            end
            uistack(prc.imageHandle, 'bottom');
        end

        function imSize = imageSize(prc, dir)
            imSize = size(prc.correctedImage, dir); 
        end            

        function changeImageSharpness(prc, radius, amount)
            prc.sharpnessAmount = amount;
            prc.sharpnessRadius = radius;
            prc.correctImage();
        end

        function correctImage(prc)            
            prc.correctedImage = imsharpen(prc.originalImage, 'Radius', prc.sharpnessRadius, 'Amount', prc.sharpnessAmount);    
            prc.showImage();
        end

        function [centers, radii] = detectFibres(prc, minRadius, maxRadius, sensitivity, threshold, method)
            I = prc.correctedImage;
            [centers,radii] = imfindcircles(I, [floor(minRadius), ceil(maxRadius)], 'ObjectPolarity', 'dark', ...
                                'Sensitivity', sensitivity, 'Method', method, 'EdgeThreshold', threshold);
        end

        function [enclosingBoundaries, enclosedBoundaries, boundaryAdjacency] = detectCoatings(prc, scaleBox)

            I = prc.correctedImage;
            BW = imbinarize(I);

            % Make ignored region completely black so there is no white
            % region detected 
            topLeftRow = round(scaleBox(2));
            topLeftCol = round(scaleBox(1));
            width = round(scaleBox(3));
            height = round(scaleBox(4));
            
            BW(topLeftRow:topLeftRow+height-1, topLeftCol:topLeftCol+width-1) = zeros(height, width); % 0 = black, 1 = white
            
            [boundaries,~, ~, A] = bwboundaries(BW,'holes');
            
            enclosingBoundaries = {};
            enclosedBoundaries = {};
            boundaryAdjacencyTemp = [];
            
            enclosingCount = 0;
            enclosedCount = 0;
            for k=1:length(boundaries)
                % check if boundary is parent boundary (no child or hole)
                % and region is bigger than a threshold
                if and(isempty(find(A(k,:),1)), area(polyshape(boundaries{k})) > 100)
                    enclosingCount = enclosingCount + 1;
                    enclosingBoundaries{enclosingCount} = flip(boundaries{k},2);
                    enclosedIndices = find(A(:,k));
                    for j=1:length(enclosedIndices)
                        enclosedCount = enclosedCount + 1;
                        enclosedBoundaries{enclosedCount} = flip(boundaries{enclosedIndices(j)},2);
                        boundaryAdjacencyTemp(enclosedCount,enclosingCount) = 1;
                    end
                end
            end

            boundaryAdjacency = zeros(enclosedCount, enclosingCount);
            boundaryAdjacency(1:size(boundaryAdjacencyTemp,1), 1:size(boundaryAdjacencyTemp,2)) = boundaryAdjacencyTemp;

        end

        function [scaleBox, scaleFactor, detectedLengthUm] = detectScaleBox(prc)
            
            I = prc.correctedImage;
            BW = imbinarize(I,0.1); % not black -> 1 (using low tolerance)
            % invert since regionprops looks for ones (white boxes)
            BW = ~BW;
            % find boxes
            stats = regionprops(BW,'BoundingBox');
            
            %find max area box -> scale box
            maxArea = 0;
            maxIndex = 1;
            for numberIndex = 1:length(stats)
                box = stats(numberIndex).BoundingBox;
                area = box(3) * box(4); % box(3) = width of the box, box(4) = height of the box (px)
                if area > maxArea
                    maxArea = area;
                    maxIndex = numberIndex;
                end
            end
            
            scaleBox = stats(maxIndex).BoundingBox;
            
            % Find text inside scale box
            ocrResult = ocr(BW, scaleBox);
            recoginzedText = ocrResult.Text;
            % Figure out what is a number and what is the unit
            numberIndex = ~isletter(recoginzedText);
            % Convert string to number
            detectedLengthUm = str2double(recoginzedText(numberIndex)); %µm            
            scaleFactor = detectedLengthUm / scaleBox(3); % Unit: µm/px 

            scaleBox = scaleBox + [-5 -5 10 10];
        end
    end
end

