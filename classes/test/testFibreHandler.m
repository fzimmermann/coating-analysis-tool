if ishandle(fig)
    close(fig)
end

global fibreHandler fibreArea

fig = figure();
ax = gca;
fibreHandler = FibreHandler(@updateCallback, ax, 1, 'on');

fibreHandler.drawFibreCircleByHand();
fibreArea = fibreHandler.FibreArea;

function updateCallback()
    global fibreArea fibreHandler
    fibreArea = fibreHandler.FibreArea;
end

 