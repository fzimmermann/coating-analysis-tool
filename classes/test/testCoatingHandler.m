global coatingHandler coatingArea
addpath('..\');

fig = figure();
ax = gca;
hold on;

coatingHandler = CoatingHandler(@updateCallback, ax);
coatingHandler.drawCoatingByHand(0);
coatingArea = coatingHandler.CoatingArea;
% coatingHandler.splitCoatingByLine();
%%
shape = coatingHandler.Coatings{1}.Shape;
hull = convhull(coatingHandler.Coatings{1}.Shape);
plot(hull);
%%
in = intersect(hull, shape);
plot(in)
%%

function updateCallback()
    global coatingArea coatingHandler
    coatingArea = coatingHandler.CoatingArea;
end

 