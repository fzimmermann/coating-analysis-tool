classdef Coating < handle
    %COATING Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Id
        EnclosingPolygon
        EnclosedPolygons
        FacePolygon
        Shape
        Area
    end
    
    methods
        function obj = Coating(id, enclosingPolygon, enclosedPolygons, face, shape, area)
            obj.Id = id;
            obj.EnclosingPolygon = enclosingPolygon;
            obj.EnclosedPolygons = enclosedPolygons;
            obj.FacePolygon = face;
            obj.Shape = shape;
            obj.Area = area;
        end

        function delete(coating)
            delete(coating.EnclosingPolygon);
            delete(coating.FacePolygon);
            for enclosedIndex = 1:length(coating.EnclosedPolygons)
                delete(coating.EnclosedPolygons{enclosedIndex});
            end
        end

        function enclosedIndex = findEnclosedPolygonIndexByTag(coating, tag)
            found = false;
            for i = 1:length(coating.EnclosedPolygons)
                if strcmp(coating.EnclosedPolygons{i}.Tag, tag)
                    enclosedIndex = i;
                    found = true;
                    break
                end
            end
            assert(found, strcat('EnclosedPolygon with Tag',tag,' not found in Coating with Id ',num2str(coating.Id)));
        end

    end
end

