classdef CoatingHandler < handle
    %COATINGHANDLER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private, GetAccess = public)
        Coatings
        NumCoatings
        CoatingArea
    end

    properties (Access = private)
        IdIncrement
        ParentUpdateCallback
        Axes
    end

    properties (Constant)
        CoatingColor = 'green';    
        CoatingFaceAlpha = 0.2;
    end

    methods
        function handler = CoatingHandler(parentUpdateCallback, axes)
            handler.ParentUpdateCallback = parentUpdateCallback;
            handler.Coatings = {};
            handler.IdIncrement = 1;
            handler.NumCoatings = 0;
            handler.CoatingArea = 0;
            handler.Axes = axes;
        end

        function delete(handler)            
            for index=1:handler.NumCoatings
                coating = handler.Coatings{index};
                delete(coating.EnclosingPolygon);
                delete(coating.FacePolygon);
                for enclosedIndex=1:length(coating.EnclosedPolygons)
                    delete(coating.EnclosedPolygons{enclosedIndex});
                end
            end
        end
        
        function coating = getCoating(handler, id)
            found = false;
            for index = 1:handler.NumCoatings
                if handler.Coatings{index}.Id == id
                    coating = handler.Coatings{index};
                    found = true;
                    break
                end
            end
            assert(found, strcat('Coating with Id ',num2str(id),' not found'));
        end

        function addCoating(handler, enclosingPolygon, enclosedPolygons, facePolygon)
            
            id = handler.IdIncrement;
            handler.IdIncrement = handler.IdIncrement + 1;

            addlistener(enclosingPolygon, 'DeletingROI', @(~,~)handler.deletingEnclosingPolygonCallback(id));
            addlistener(enclosingPolygon, 'ROIMoved', @(~,movedPolygon)handler.movedEnclosingPolygonCallback(id,movedPolygon));

            for enclosedIndex=1:length(enclosedPolygons)
                enclosedPolygon = enclosedPolygons{enclosedIndex};
                addlistener(enclosedPolygon, 'DeletingROI', @(deletedPolygon,~)handler.deletingEnclosedPolygonCallback(id,deletedPolygon));
                addlistener(enclosedPolygon, 'ROIMoved', @(~,movedPolygon)handler.movedEnclosedPolygonCallback(id,movedPolygon));
            end

            shape = facePolygon.Shape;
            coatingArea = area(shape);

            coating = Coating(id, enclosingPolygon, enclosedPolygons, facePolygon, shape, coatingArea);
            handler.Coatings{end+1} = coating;

            handler.NumCoatings = handler.NumCoatings + 1;
            handler.CoatingArea = handler.CoatingArea + coatingArea;

            handler.ParentUpdateCallback('AddedCoating', coating);
        end



        function deleteCoating(handler, id)
            coatingIndex = handler.findCoatingIndex(id);
            coating = handler.Coatings{coatingIndex};

            delete(coating.EnclosingPolygon);
            delete(coating.FacePolygon);
            for enclosedIndex=1:length(coating.EnclosedPolygons)
                delete(coating.EnclosedPolygons{enclosedIndex});
            end

            handler.Coatings(coatingIndex) = [];
            handler.NumCoatings = handler.NumCoatings - 1;
            handler.CoatingArea = handler.CoatingArea - coating.Area;

            handler.ParentUpdateCallback('DeletedCoating', coating);
        end

        function drawCoatingByHand(handler, numHoles)
            enclosingPolygon = drawpolygon('Parent', handler.Axes, 'Color', handler.CoatingColor, 'FaceAlpha', 0);

            faceShape = polyshape(enclosingPolygon.Position);

            numEnclosed = numHoles;
            enclosedPolygons = cell(numEnclosed,1);
            for i=1:numEnclosed
                enclosedPolygon = drawpolygon('Parent', handler.Axes, 'Color', handler.CoatingColor, 'FaceAlpha', 0, 'Tag', num2str(i));
                enclosedPolygons{i} = enclosedPolygon;
                faceShape = subtract(faceShape, polyshape(enclosedPolygon.Position));
            end
            
            facePolygon = handler.plotCoatingFace(faceShape);

            handler.addCoating(enclosingPolygon, enclosedPolygons, facePolygon);
        end

        function drawCoatingFromInput(handler, enclosingBorder, enclosedBorders)
            enclosingPolygon = drawpolygon('Parent', handler.Axes, 'Position', reducepoly(enclosingBorder, 0.02),...
                'Color', handler.CoatingColor, 'FaceAlpha', 0);
            
            faceShape = polyshape(enclosingPolygon.Position);

            numEnclosed = length(enclosedBorders);
            enclosedPolygons = cell(numEnclosed,1);
            for i=1:numEnclosed
                enclosedPolygon = drawpolygon('Parent', handler.Axes, 'Position', reducepoly(enclosedBorders{i}, 0.02),...
                'Color', handler.CoatingColor, 'FaceAlpha', 0, 'Tag', num2str(i));
                enclosedPolygons{i} = enclosedPolygon;
                faceShape = subtract(faceShape, polyshape(enclosedPolygon.Position));
            end
            
            facePolygon = handler.plotCoatingFace(faceShape);

            handler.addCoating(enclosingPolygon, enclosedPolygons, facePolygon);
            
        end

        function splitCoatingByLine(handler)
            line = drawline('Parent', handler.Axes, 'Color', 'k');
            for index = 1:handler.NumCoatings
                % find coating which intersects with the drawn line
                coating = handler.Coatings{index};
                [in,~] = intersect(coating.Shape, line.Position);
                if ~isempty(in) 
                    % split the shape defined by enclosing polygon into
                    % parts along drawn line
                    parts = handler.cutShapeByLine(polyshape(coating.EnclosingPolygon.Position), line);
                    assert(length(parts) == 2);
                    
                    numEnclosed = length(coating.EnclosedPolygons);
                    if numEnclosed > 0                        
                        enclosedBorders1 = {};
                        enclosedBorders2 = {};
                        for enclosedIndex = 1:numEnclosed
                            % check which hole in the original coating belongs
                            % to which part
                            enclosedShape = polyshape(coating.EnclosedPolygons{enclosedIndex}.Position);
                            in1 = intersect(parts(1), enclosedShape);
                            in2 = intersect(parts(2), enclosedShape);
                            if ~isempty(in1.Vertices)
                                enclosedBorders1{end+1} = enclosedShape.Vertices;
                            elseif ~isempty(in2.Vertices)
                                enclosedBorders2{end+1} = enclosedShape.Vertices;
                            end
                        end
                        handler.drawCoatingFromInput(parts(1).Vertices, enclosedBorders1);
                        handler.drawCoatingFromInput(parts(2).Vertices, enclosedBorders2)
                    else
                        handler.drawCoatingFromInput(parts(1).Vertices, {});
                        handler.drawCoatingFromInput(parts(2).Vertices, {});
                    end
                    
                    handler.deleteCoating(coating.Id);     
                    break
                end
            end
            delete(line);
        end

    end

    methods (Access=private)
        
        function deletingEnclosingPolygonCallback(handler, id)     
            handler.deleteCoating(id);
        end

        function movedEnclosingPolygonCallback(handler, id, movedPolygon)
            coating = handler.getCoating(id);
            delete(coating.FacePolygon);

            handler.CoatingArea = handler.CoatingArea - coating.Area;
            
            changedPosition = movedPolygon.CurrentPosition(1,:) - movedPolygon.PreviousPosition(1,:);
            faceShape = polyshape(movedPolygon.CurrentPosition);
            for enclosedIndex=1:length(coating.EnclosedPolygons)
                enclosedPolygon = coating.EnclosedPolygons{enclosedIndex};
                enclosedPolygon.Position = enclosedPolygon.Position + changedPosition;
                faceShape = subtract(faceShape, polyshape(enclosedPolygon.Position));
            end
            facePolygon = handler.plotCoatingFace(faceShape);
            coating.FacePolygon = facePolygon;
            coating.Shape = faceShape;
            coating.Area = area(faceShape);    

            handler.CoatingArea = handler.CoatingArea + coating.Area;

            handler.ParentUpdateCallback('MovedCoating', coating);
        end

        function deletingEnclosedPolygonCallback(handler, id, deletedPolygon)
            coating = handler.getCoating(id);

            handler.CoatingArea = handler.CoatingArea - coating.Area;
            
            delete(coating.FacePolygon);
            enclosedIndex = coating.findEnclosedPolygonIndexByTag(deletedPolygon.Tag);
            coating.EnclosedPolygons(enclosedIndex) = [];

            coating.Shape = union(coating.Shape, polyshape(deletedPolygon.Position));
            coating.Area = area(coating.Shape);
            coating.FacePolygon = handler.plotCoatingFace(coating.Shape);

            handler.CoatingArea = handler.CoatingArea + coating.Area;

            handler.ParentUpdateCallback('MovedCoating', coating);
        end

        function movedEnclosedPolygonCallback(handler, id, movedPolygon)
            coating = handler.getCoating(id);
            
            delete(coating.FacePolygon);
            handler.CoatingArea = handler.CoatingArea - coating.Area;
            
            coating.Shape = subtract(union(coating.Shape, intersect(polyshape(coating.EnclosingPolygon.Position), polyshape(movedPolygon.PreviousPosition))), polyshape(movedPolygon.CurrentPosition));
            coating.Area = area(coating.Shape);
            coating.FacePolygon = handler.plotCoatingFace(coating.Shape);

            handler.CoatingArea = handler.CoatingArea + coating.Area;

            handler.ParentUpdateCallback('MovedCoating', coating);
        end

        function index = findCoatingIndex(handler, id)
            %Get Coating Index inside of Coating cell array (Index ~= Id)
            found = false;
            for i = 1:handler.NumCoatings
                if handler.Coatings{i}.Id == id
                    index = i;
                    found = true;
                    break
                end
            end
            assert(found, strcat('Coating with Id ',num2str(id),' not found'));
        end

        function facePolygon = plotCoatingFace(handler,faceShape)
            hold(handler.Axes,'on');
            facePolygon = plot(handler.Axes, faceShape, 'FaceColor', handler.CoatingColor,...
                'FaceAlpha', handler.CoatingFaceAlpha, 'EdgeColor', 'none');
            hold(handler.Axes,'off');
            uistack(facePolygon,'bottom');
            uistack(facePolygon,'up');
        end    

        function parts = cutShapeByLine(~, shape, line)
            eps = 1e-3;
            x = line.Position(:,1);
            y = line.Position(:,2);
            lineShape = polyshape([x(1)+eps y(1)+eps; x(1)-eps y(1)-eps; x(2)+eps y(2)+eps; x(2)-eps y(2)-eps]);
            
            shape = subtract(shape, lineShape);
            parts = regions(shape);
        end
    end
end

