classdef FibreCoatingAdjacencyHandler < handle
    
    properties
        fibreHandler
        coatingHandler
        adjacency % array holding pairs of fibreId and coatingId, e.g [a b; c d; e f] meaning fibre a is coated by coating b, fibre c is coated by coating d etc.
    end
    
    methods
        function handler = FibreCoatingAdjacencyHandler(fibreHandler, coatingHandler)
            handler.coatingHandler = coatingHandler;
            handler.fibreHandler = fibreHandler;
            handler.adjacency = [];
        end
        
        function updateFibreAdjacencies(handler, fibre)
            handler.deleteFibreAdjacencies(fibre);

            numCoatings = handler.coatingHandler.NumCoatings;
            for coatingIndex = 1:numCoatings
                coating = handler.coatingHandler.Coatings{coatingIndex};
                if handler.checkFibreCoatingAdjacency(fibre, coating)
                    handler.adjacency = [handler.adjacency; fibre.Id coating.Id];
                end
            end
        end

        function deleteFibreAdjacencies(handler, fibre)
            index = 1;
            while index <= size(handler.adjacency, 1)
                if handler.adjacency(index,1) == fibre.Id
                    handler.adjacency(index,:) = [];
                else 
                    index = index + 1;
                end
            end
        end

        function updateCoatingAdjacencies(handler, coating)
            handler.deleteCoatingAdjacencies(coating);

            numFibres = handler.fibreHandler.NumFibres;
            for fibreIndex = 1:numFibres
                fibre = handler.fibreHandler.Fibres{fibreIndex};
                if handler.checkFibreCoatingAdjacency(fibre, coating)
                    handler.adjacency = [handler.adjacency; fibre.Id coating.Id];
                end
            end
        end

        function deleteCoatingAdjacencies(handler, coating)
            index = 1;
            while index <= size(handler.adjacency, 1)
                if handler.adjacency(index,2) == coating.Id
                    handler.adjacency(index,:) = [];
                else 
                    index = index + 1;
                end
            end
        end

        function ratio = getFullyCoatedRatio(handler)
            countFullyCoated = 0;
            numAdjacencies = size(handler.adjacency,1);
            for k=1:numAdjacencies
                coatingId = handler.adjacency(k,2);
                countFullyCoated = countFullyCoated + handler.checkFullyCoated(coatingId);
            end
            ratio = countFullyCoated / max(1,numAdjacencies) * 100;
        end

        function avgThickness = getAverageCoatingThickness(handler)
            avgThickness = 0;
            numAdjacencies = size(handler.adjacency,1);
            for k = 1:numAdjacencies
                fibreId = handler.adjacency(k,1);
                coatingId = handler.adjacency(k,2);
                avgThickness = avgThickness + handler.getCoatingThickness(fibreId, coatingId);
            end
            avgThickness = avgThickness / max(1,numAdjacencies);
        end

        function thickness = getCoatingThickness(handler,fibreId,coatingId)   
            % we find bounding boxes of the fibre and the coating and
            % determine the distance of the bounds in each direction 
            % (+x, -x, y, -y). If the bounding box of the fibre lays
            % outisde the bounding box of the coating in one direction we
            % consider the fibre not to be coated in that direction and the
            % thickness is zero in that direction. The overall coating
            % thickness is the mean of determined distances in each
            % direction

            fibre = handler.fibreHandler.getFibre(fibreId);
            coating = handler.coatingHandler.getCoating(coatingId); 

            [xlimFibre, ylimFibre] = boundingbox(fibre.Shape);
            [xlimCoating, ylimCoating] = boundingbox(coating.Shape);

            % left = 1; right = 2; top = 3; bottom = 4;
            thickness = mean([max(0, xlimFibre(1) - xlimCoating(1));
                         max(0, xlimCoating(2) - xlimFibre(2));
                         max(0, ylimCoating(2) - ylimFibre(2));
                         max(0, ylimFibre(1) - ylimCoating(1))]);       
        end

        function removeSingleObjects(handler)
            % Removes every coating which is not adjacent to a fibre and
            % every fibre not adjacent to a coating

            fibreIndex = 1;
            while fibreIndex <= handler.fibreHandler.NumFibres
                fibre = handler.fibreHandler.Fibres{fibreIndex};
                if ~ismember(fibre.Id, handler.adjacency(:,1))
                    handler.fibreHandler.deleteFibre(fibre.Id);
                else
                    fibreIndex = fibreIndex + 1;
                end
            end

            coatingIndex = 1;
            while coatingIndex <= handler.coatingHandler.NumCoatings
                coating = handler.coatingHandler.Coatings{coatingIndex};
                if ~ismember(coating.Id, handler.adjacency(:,2))
                    handler.coatingHandler.deleteCoating(coating.Id);
                else
                    coatingIndex = coatingIndex + 1;
                end
            end
        end


    end

    methods(Access=private)
        function result = checkFibreCoatingAdjacency(~, fibre, coating)
        % check if convex hull of coating intersects with fibre. If thats
        % the case, the fibre is connected to the coating the function
        % returns true, otherwise false 
            coatingHull = convhull(coating.Shape);            
            in = intersect(coatingHull, fibre.Shape);
            result = ~isempty(in.Vertices);
        end

        function result = checkFullyCoated(handler, coatingId)
        % if a coating is defined by an enclosing and enclosed polygon (has
        % a hole) we consider the fibre (inside the hole) to be fully
        % coated
            coating = handler.coatingHandler.getCoating(coatingId);
            result = ~isempty(coating.EnclosedPolygons);          
        end

    end
end

