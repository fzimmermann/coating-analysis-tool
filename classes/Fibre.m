classdef Fibre < handle
    %FIBRE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Id
        Type % Circle or Polygon
        ROIObject
        Shape
        Area
    end
    
    methods
        function obj = Fibre(id, type, roiobject, shape, area)
            %FIBRE Construct an instance of this class
            %   Detailed explanation goes here
            obj.Id = id;
            obj.Type = type;
            obj.ROIObject = roiobject;
            obj.Shape = shape;
            obj.Area = area;
        end

        function delete(fibre)
            delete(fibre.ROIObject);
        end
    end
end

