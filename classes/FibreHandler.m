classdef FibreHandler < handle
    
    properties (GetAccess=public, SetAccess=private)
        Fibres
        NumFibres
        FibreArea
    end

    properties(Access=private)    
        IdIncrement
        ParentUpdateCallback
        ScaleFactor % px -> µm
        LabelVisible
        Axes
        CircleDiameters
    end

    properties (Constant)
        FibreColor = 'red';    
        FibreFaceAlpha = 0.2;
    end
    
    methods
        
        function handler = FibreHandler(parentUpdateCallback, axes, scaleFactor)

            handler.Fibres = {};
            handler.IdIncrement = 1;
            handler.ParentUpdateCallback = parentUpdateCallback;
            handler.NumFibres = 0;
            handler.ScaleFactor = scaleFactor;             
            handler.FibreArea = 0;
            handler.Axes = axes;
            handler.CircleDiameters = [];
        end

        function delete(handler)            
            for index=1:handler.NumFibres
                delete(handler.Fibres{index}.ROIObject);
            end
        end
        
        function fibre = getFibre(handler, id)
            found = false;
            for i = 1:length(handler.Fibres)
                if handler.Fibres{i}.Id == id
                    fibre = handler.Fibres{i};
                    found = true;
                    break
                end
            end
            assert(found, strcat('Fibre with Id ',num2str(id),' not found'));
        end
        
        function addFibreCircle(handler, circle)
            
            id = handler.IdIncrement;
            handler.IdIncrement = handler.IdIncrement + 1;

            shape = polyshape(circle.Vertices);
            fibreArea = area(shape);
            addlistener(circle, 'DeletingROI', @(~,~)handler.deletingFibreCallback(id));
            addlistener(circle, 'ROIMoved', @(~, movedCircle)handler.movedFibreCircleCallback(id, movedCircle));

            diameter = round(2*circle.Radius*handler.ScaleFactor);
            set(circle, 'Label', strcat(num2str(diameter),' µm'));
            handler.addDiameterToCircleDiameters(diameter);
            
            fibre = Fibre(id, 'Circle', circle, shape, fibreArea);
            handler.Fibres{end+1} = fibre;

            handler.NumFibres = handler.NumFibres + 1;
            handler.FibreArea = handler.FibreArea + fibreArea;            

            handler.ParentUpdateCallback('AddedFibre', fibre);
        end
        
        function drawFibreCircleFromInput(handler, center, radius)
            circle = drawcircle('Parent', handler.Axes,...
                'Color', handler.FibreColor, 'FaceAlpha', handler.FibreFaceAlpha,...
                'Center', center, 'Radius', radius);
            handler.addFibreCircle(circle);
        end

        function drawFibreCircleByHand(handler)
            circle = drawcircle('Parent', handler.Axes, 'Color', handler.FibreColor, 'FaceAlpha', handler.FibreFaceAlpha);
            handler.addFibreCircle(circle);
        end

        function addFibrePolygon(handler, polygon)
            
            id = handler.IdIncrement;
            handler.IdIncrement = handler.IdIncrement + 1;

            shape = polyshape(polygon.Position);
            fibreArea = area(shape);
            addlistener(polygon, 'DeletingROI', @(~,~)handler.deletingFibreCallback(id));
            addlistener(polygon, 'ROIMoved', @(movedPolygon,~)handler.movedFibrePolygonCallback(id, movedPolygon));
            
            fibre = Fibre(id, 'Polygon', polygon, shape, fibreArea);
            handler.Fibres{end+1} = fibre;
            handler.NumFibres = handler.NumFibres + 1;
            handler.FibreArea = handler.FibreArea + fibreArea;

            handler.ParentUpdateCallback('AddedFibre', fibre);
        end        

        function drawFibrePolygonByHand(handler)
            polygon = drawpolygon('Parent', handler.Axes, 'Color', handler.FibreColor, 'FaceAlpha', handler.FibreFaceAlpha);
            handler.addFibrePolygon(polygon);
        end

        function drawFibrePolygonFromInput(handler, position)
            polygon = drawpolygon('Parent', handler.Axes, 'Position', position,...
                'Color', handler.FibreColor, 'FaceAlpha', handler.FibreFaceAlpha);
            handler.addFibrePolygon(polygon);
        end

        function deleteFibre(handler, id)
            index = handler.findFibreIndex(id);
            fibre = handler.Fibres{index};
            
            handler.Fibres(index) = [];
            handler.NumFibres = handler.NumFibres - 1;
            handler.FibreArea = handler.FibreArea - fibre.Area;

            if strcmp(fibre.Type, 'Circle')
                diameter = round(2 * fibre.ROIObject.Radius * handler.ScaleFactor);
                handler.removeDiameterFromCircleDiameters(diameter);
            end            

            delete(fibre.ROIObject);
            handler.ParentUpdateCallback('DeletedFibre', fibre);
        end

        function setScaleFactor(handler, scaleFactor)
            handler.ScaleFactor = scaleFactor;
            handler.CircleDiameters = [];
            for fibreIndex = 1:handler.NumFibres
                fibre = handler.Fibres{fibreIndex};
                if strcmp(fibre.Type, 'Circle')
                    diameter = round(2*fibre.ROIObject.Radius*scaleFactor);
                    set(fibre.ROIObject, 'Label', strcat(num2str(diameter),' µm'));
                    handler.addDiameterToCircleDiameters(diameter);
                end
            end
        end

        function maxDiameter = getMaxFibreDiameter(handler)
            if length(handler.CircleDiameters) > 0
                maxDiameter = max(handler.CircleDiameters);
            else
                maxDiameter = 0;
            end
        end

        function minDiameter = getMinFibreDiameter(handler)
            if length(handler.CircleDiameters) > 0
                minDiameter = min(handler.CircleDiameters);
            else
                minDiameter = 0;
            end
        end

        function meanDiameter = getMeanFibreDiameter(handler)
            if length(handler.CircleDiameters) > 0
                meanDiameter = round(mean(handler.CircleDiameters));
            else
                meanDiameter = 0;
            end             
        end

    end

    methods (Access=private)

        function deletingFibreCallback(handler, id)            
            handler.deleteFibre(id);
        end

        function movedFibreCircleCallback(handler, id, movedCircle)            
            fibre = handler.getFibre(id);

            handler.FibreArea = handler.FibreArea - fibre.Area;            
            fibre.Shape = polyshape(movedCircle.Source.Vertices);
            fibre.Area = area(fibre.Shape);

            previousDiameter = round(2*movedCircle.PreviousRadius*handler.ScaleFactor);
            currentDiameter = round(2*movedCircle.CurrentRadius*handler.ScaleFactor);
            set(movedCircle.Source, 'Label', strcat(num2str(currentDiameter),' µm'));

            handler.FibreArea = handler.FibreArea + fibre.Area;
            handler.updateDiameterInCircleDiameters(previousDiameter, currentDiameter);

            handler.ParentUpdateCallback('MovedFibre', fibre);
        end

        function movedFibrePolygonCallback(handler, id, movedPolygon)
            fibre = handler.getFibre(id);
            
            handler.FibreArea = handler.FibreArea - fibre.Area;
            fibre.Shape = polyshape(movedPolygon.Position);
            fibre.Area = area(fibre.Shape);
            handler.FibreArea = handler.FibreArea + fibre.Area;

            handler.ParentUpdateCallback('MovedFibre', fibre); 
        end

        function index = findFibreIndex(handler, id)
            %Get Fibre Index inside of Fibres cell array (Index ~= Id)
            found = false;
            for i = 1:length(handler.Fibres)
                if handler.Fibres{i}.Id == id
                    index = i;
                    found = true;
                    break
                end
            end
            assert(found, strcat('Fibre with Id ',num2str(id),' not found'));
        end
           
        function removeDiameterFromCircleDiameters(handler, diameter)
            handler.CircleDiameters(find(handler.CircleDiameters == diameter, 1)) = [];
        end

        function addDiameterToCircleDiameters(handler, diameter)
            handler.CircleDiameters = [handler.CircleDiameters diameter];
        end

        function updateDiameterInCircleDiameters(handler, previousDiameter, currentDiameter)
            handler.CircleDiameters(find(handler.CircleDiameters == previousDiameter, 1)) = currentDiameter;
        end

    end

end

